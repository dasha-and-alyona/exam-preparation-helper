from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem.snowball import SnowballStemmer
import numpy as np
import math


def split_sentences(text1, text2):
    text1 = text1.replace(',', ' ').replace(". ", ".").split(".")
    text2 = text2.replace(',', ' ').replace(". ", ".").split(".")
    _text1 = []
    _text2 = []
    for sentence in text1:
        words = sentence.split(' ')
        for word in words:
            _text1.append(word)
    for sentence in text2:
        words = sentence.split(' ')
        for word in words:
            _text2.append(word)

    return _text1, _text2


def stem_words(_text1, _text2):
    stemmer = SnowballStemmer("russian")
    l1 = [stemmer.stem(word) for word in _text1]
    l2 = [stemmer.stem(word) for word in _text2]
    sentence1 = " ".join(str(i) for i in l1)
    sentence2 = " ".join(str(i) for i in l2)
    return sentence1, sentence2


def find_similarity(text1, text2):
    words = split_sentences(text1, text2)
    stemmed = stem_words(words[0], words[1])
    vect = TfidfVectorizer(min_df=1)
    tfidf = vect.fit_transform([stemmed[0], stemmed[1]])
    result = (tfidf * tfidf.T).A
    return round(result[0][1], 2)
