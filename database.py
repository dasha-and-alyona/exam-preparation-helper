from pymongo import MongoClient
from mongoengine import *
import datetime
import io
import docx
from convert_docx_to_html import *

client = MongoClient('mongodb://localhost:27017')
db = client['examix_bot']
chats = db.chats


class Chat(Document):
    chat_id = StringField(required=True)
    file_id = StringField(default=None)
    document = FileField(default=None)


def find_chat(chat_id):
    chat = chats.find_one({"chat_id": chat_id})
    return chat


def get_question_for_chat(chat_id):
    chat = find_chat(chat_id)
    if chat is None:
        add_chat(chat_id)
        return "Вы не добавили файл для работы, пожалуйста, добавьте его."
    if 'document' not in chat or chat['document'] is None:
        return "Вы не добавили файл для работы. Пожалуйста, пришлите боту документ с вопросами в формате .docx"

    stream = io.BytesIO()
    stream.write(chat['document'])
    result = get_question(stream)
    stream.close()
    return result


def add_doc_to_chat(chat_id, file_id, file):
    chat = find_chat(chat_id)
    if chat is None:
        new_chat = {
            'chat_id': chat_id,
            'file_id': file_id,
            'document': file
        }
        chats.insert_one(new_chat)
    else:
        db.chats.update_one({"chat_id": chat_id}, {"$set": {"file_id": file_id, "document": file}})


def add_chat(chat_id):
    new_chat = {
        'chat_id': chat_id
    }
    chats.insert_one(new_chat)


def save_question_and_answer(chat_id, element):
    db.chats.update_one({"chat_id": chat_id}, {"$set": {"question": element[0], "answer": element[1]}})


def is_answer(chat_id):
    chat = find_chat(chat_id)
    if 'answer' not in chat or chat['answer'] is None:
        return False
    return True


def find_answer(chat_id):
    chat = find_chat(chat_id)
    return chat['answer']


def delete_answer(chat_id):
    db.chats.update_one({"chat_id": chat_id}, {"$set": {"question": None, "answer": None}})

