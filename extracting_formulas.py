"""
вроде как этот код должен вытягивать формулы, но он не работает, и я не до конца понимаю, как он работает
"""
try:
    from xml.etree.cElementTree import XML
except ImportError:
    from xml.etree.ElementTree import XML
import zipfile
import sys

"""
Extracts formulas from docx document
Created to detect malicious docx documents containing DDE formulas

Modified @etienned's docx text extracting python script:
https://gist.github.com/etienned/7539105
"""

WORD_NAMESPACE = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
PARA = WORD_NAMESPACE + 'p'
TEXT = WORD_NAMESPACE + 'instrText'


def get_docx_formulas(path):
    document = zipfile.ZipFile('demo1.docx')
    xml_content = document.read('word/document.xml')
    document.close()

    tree = XML(xml_content)

    print(tree)

    paragraphs = []
    for paragraph in tree.getiterator(PARA):
        texts = [node.text
                 for node in paragraph.getiterator(TEXT)
                 if node.text]
        if texts:
            paragraphs.append(''.join(texts))

    return '\n\n'.join(paragraphs)


if __name__ == "__main__":

    try:
        print(get_docx_formulas(sys.argv[1]))
    except:
        print(str('usage: ' + sys.argv[0]) + ' [docx file]')

print(get_docx_formulas('demo1.docx'))