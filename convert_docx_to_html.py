import mammoth
import bs4
import re
import random

# with open("demo1.docx", "rb") as docx_file:
#     result = mammoth.convert_to_html('C:\\Users\\alyona\\Desktop\\Test.docx')
#     html = result.value # The generated HTML
#     messages = result.messages # Any messages, such as warnings during conversion
#     print(html)
#
#
# result = mammoth.convert_to_html('C:\\Users\\alyona\\Desktop\\3 страницы пример.docx')
# html = result.value  # The generated HTML
# messages = result.messages  # Any messages, such as warnings during conversion
# print(html)
# soup = bs4.BeautifulSoup(html, features="lxml")
# list_of_questions = []
# for heading in soup.find_all(re.compile("^h[1-6]")):
#     answ = ''
#     for p in heading.next_siblings:
#         if p.name in ['p', 'em', 'ul', 'li', 'strong', 'br', 'ol', 'sup']:
#             answ += p.text.replace('\t', ' ').strip() + ' '
#         else:
#             break
#     list_of_questions.append((heading.text.strip(), answ))
#     k = random.randint(1, len(list_of_questions))
# print(list_of_questions)


def get_question(file):
    result = mammoth.convert_to_html(file)
    html = result.value
    soup = bs4.BeautifulSoup(html, features="lxml")
    list_of_questions = []
    for heading in soup.find_all(re.compile("^h[1-6]")):
        answer = ''
        for p in heading.next_siblings:
            # if p.name == 'p':
            if p.name in ['p', 'em', 'ul', 'li', 'strong', 'br', 'ol', 'sup']:
                answer += p.text.replace('\t', ' ').strip() + ' '
            else:
                break
        list_of_questions.append((heading.text.strip(), answer))

    k = random.randint(0, len(list_of_questions) - 1)
    if len(list_of_questions) != 0:
        return list_of_questions[k]
    else:
        return ("Не удалось извлечь вопросы", "")
