NS = {
    'm': 'http://schemas.openxmlformats.org/officeDocument/2006/math'
}


def m_t(element):
    return element.text


def m_lim_low(element):
    result = ''

    for child in element.getchildren():
        name = child.prefix + ':' + child.tag.split('}')[-1]
        if name != 'm:lim':
            result += convert_element(child)
        else:
            result += f" ({convert_element(child)})"

    # return result + ' '
    return result


def m_nary(element):
    sign = element.xpath('m:naryPr/m:chr/@m:val', namespaces=NS)[0]
    sub = convert_all(element.xpath('m:sub', namespaces=NS))
    sup = convert_all(element.xpath('m:sup', namespaces=NS))
    # return f"{sign} ({sub} to {sup}) "
    return f"{sign}({sub}to{sup})"

def m_s_sub(element):
    reg = convert_all(element.xpath('m:e', namespaces=NS))
    sub = convert_all(element.xpath('m:sub', namespaces=NS))
    # return f"{reg}_{sub}" + " "
    return f"{reg}_{sub}"


def m_s_sup(element):
    reg = convert_all(element.xpath('m:e', namespaces=NS))
    sup = convert_all(element.xpath('m:sup', namespaces=NS))
    # return f"{reg}^{sup}" + " "
    return f"{reg}^{sup}"


def m_rad(element):
    deg = convert_all(element.xpath('m:deg', namespaces=NS))
    if deg == '2' or deg == '':
        deg = 'sq'

    # return f"{deg}rt {convert_all(element.xpath('m:e', namespaces=NS))}"
    return f"{deg}rt{convert_all(element.xpath('m:e', namespaces=NS))}"


CHILDREN = {
    'm:t': m_t,
    'm:limLow': m_lim_low,
    'm:nary': m_nary,
    'm:sSub': m_s_sub,
    'm:sSup': m_s_sup,
    'm:rad': m_rad
}


def convert_element(element):
    name = element.prefix + ':' + element.tag.split('}')[-1]
    result = ''

    if name in CHILDREN:
        result = CHILDREN[name](element)
    elif len(element.getchildren()) > 0:
        for child in element.getchildren():
            result += convert_element(child)

    return result


def convert_all(elements):
    return ''.join(convert_element(e) for e in elements)
