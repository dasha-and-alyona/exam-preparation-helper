import zipfile
from io import StringIO

from lxml import etree

from children import convert_element
# import element tree
import xml.etree.ElementTree as ET


# replacement = StringIO(
#     '<w:r w:rsidDel="00000000" w:rsidR="00000000" w:rsidRPr="00000000"><w:rPr><w:rtl w:val="0"/></w:rPr><w:t xml:space="preserve"> . </w:t></w:r>')
# tree = etree.parse(replacement)
# print(tree)


def convert_formula(element):
    result = convert_element(element)

    signs = {
        '∫': 'int',
        '∑': 'sum',
        '∞': 'inf',
        '→': '->',
        'π': 'pi',
        'μ': 'mu',
        '⋅': '*'
    }

    for sym, rep in signs.items():
        result = result.replace(sym, rep)

    return result


def parse_document_xml(document_name):
    with open(document_name, 'rb') as f:
        xml = f.read()

    root = etree.fromstring(xml)
    ns = root.nsmap

    formulae = root.xpath('/w:document/w:body/*/m:oMath', namespaces=ns)

    result = map(convert_formula, formulae)
    return result


if __name__ == "__main__":
    formulas = parse_document_xml('document.xml')

    for formula in formulas:
        print(formula)

    # tree = ET.parse('document.xml')
    # print(tree)
    # root = tree.getroot()
    # print(root)
    # elem = tree.findall('w:document')
    # print(elem)

    # mydoc = zipfile.ZipFile('C:\\Users\\alyona\\Desktop\\Test.docx')
    # formula = "a+b"
    # xmlcontent = mydoc.read('word/document.xml')
    # document = etree.fromstring(xmlcontent)
    # ns = document.nsmap
    #
    # formulae = document.xpath('/w:document/w:body/*/m:oMath', namespaces=ns)
    # for f in fo
    # print(formulae)


