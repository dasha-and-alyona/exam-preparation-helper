import telebot
import docx
import logging
import io
from database import *
from mongoengine import *
from nlp import find_similarity

# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# logger = telebot.logger
# telebot.logger.setLevel(logging.DEBUG)

bot = telebot.TeleBot("715157528:AAFxolZPCuZm0i4yIThzJoqydyZ9vS1zPIc")

connect('examix_bot', host='localhost', port=27017)

keyboard1 = telebot.types.ReplyKeyboardMarkup(True)
keyboard1.row('/help', '/rules')
keyboard1.row('/prepare')


def get_rules():
    rules = "Загружаемый документ должен быть формата .docx. Вопросы (темы) должны быть выделены как заголовки, а ответы к ним — обычным текстом."
    return rules


@bot.message_handler(commands=['start', 'help'])
def handle_first_message(message):
    chat_id = message.chat.id
    chat = find_chat(chat_id)

    if chat is None:
        add_chat(chat_id)

    bot.send_message(chat_id, "Загрузите документ с форматированием, как указано в /rules. После загрузки документа вы получите уведомление. Чтобы получить вопрос, используйте команду /prepare.", reply_markup=keyboard1)


@bot.message_handler(commands=['rules'])
def handle_rules_command(message):
    chat_id = message.chat.id
    result = get_rules()
    bot.send_message(chat_id, result)


@bot.message_handler(commands=['prepare'])
def send_question(message):
    chat_id = message.chat.id
    result = get_question_for_chat(chat_id)
    save_question_and_answer(chat_id, result)
    bot.send_message(chat_id, result)


@bot.message_handler(content_types=['document'])
def handle_docs(message):
    chat_id = message.chat.id
    doc = message.document

    file_info = bot.get_file(doc.file_id)
    if file_info.file_path.endswith('.docx'):
        downloaded_file = bot.download_file(file_info.file_path)
        add_doc_to_chat(chat_id, doc.file_id, downloaded_file)

        # stream = io.BytesIO()
        # stream.write(downloaded_file)
        # doc = docx.Document(stream)
        # questions = get_question(stream)
        # stream.close()
        # paragraphs_count = len(doc.paragraphs)

        bot.send_message(chat_id,
                         f"Документ сохранён. Чтобы начать отвечать на вопросы, нажмите /prepare.")
        result = get_question_for_chat(chat_id)
        save_question_and_answer(chat_id, result)
        bot.send_message(chat_id, result[0])
    else:
        bot.send_message(chat_id,
                         f"Документ должен быть в формате docx. Посмотреть все правила оформления можно по команде /rules.")


@bot.message_handler(func=lambda m: True)
def echo_all(message):
    chat_id = message.chat.id
    if is_answer(chat_id):
        answer = find_answer(chat_id)
        result = find_similarity(message.text, answer)
        bot.send_message(chat_id,
                         f"Коэффициент сходства с ответом из файла равен {result}. Отпрвьте /prepare, чтобы ответить на следующий вопрос.")
        delete_answer(chat_id)
    else:
        bot.reply_to(message,
                     "Отправьте /help для того, чтобы узнать, что делает бот, или /rules, чтобы узнать прафила оформления документов.")

    # logger = logging.getLogger()
    # logger.setLevel(logging.INFO)
    # logger.setLevel(logging.DEBUG)


bot.polling()
